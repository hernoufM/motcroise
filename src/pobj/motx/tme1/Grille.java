package pobj.motx.tme1;

/**
 * Classe représentant une grille qui est implémentée comme une matrice de cases
 * 
 * @author 3407108 3872505
 *
 */
public class Grille {
	/**
	 * Matrice de cases
	 */
	private Case[][] grille;

	/**
	 * Construit une grille en initialisant chacune de ses cases comme étant vides
	 * @param hauteur nombre de lignes d'une matrice
	 * @param largeur nombre de colonnes d'une matrice
	 */
	public Grille(int hauteur, int largeur) {
		grille = new Case[hauteur][largeur];
		for (int i = 0; i < hauteur; i++) {
			for (int j = 0; j < largeur; j++) {
				grille[i][j] = new Case(i, j, ' ');
			}
		}
	}

	/**
	 * Retourne la case à la position (i,j)
	 * @param lig numéro de ligne i
	 * @param col numéro de colonne j
	 * @return la case à la position (i,j)
	 */
	public Case getCase(int lig, int col) {
		return grille[lig][col];
	}

	/**
	 * Représentation textuelle d'une grille
	 * 
	 * @return la représentation textuelle d'une grille avec un espace entre chaque case
	 */
	@Override
	public String toString() {
		return GrilleLoader.serialize(this, false);
	}

	/**
	 * Retourne le nombre de lignes d'une grille
	 * 
	 * @return le nombre de lignes d'une grille
	 */
	public int nbLig() {
		return grille.length;
	}

	/**
	 * Retourne le nombre de colonnes d'une grille
	 * 
	 * @return le nombre de colonnes d'une grille
	 */
	public int nbCol() {
		return grille[0].length;
	}

	/**
	 * Créé une copie d'une grille ainsi que chacune de ses cases
	 * 
	 * @return une copie d'une grille
	 */
	public Grille copy() {
		Grille g = new Grille(this.nbLig(), this.nbCol());
		for (int i = 0; i < this.nbLig(); i++) {
			for (int j = 0; j < this.nbCol(); j++) {
				g.grille[i][j] = new Case(i, j, grille[i][j].getChar());
			}
		}
		return g;
	}
}
