package pobj.motx.tme1;
import java.util.*;
/**
 * Classe représentant un mot dans une grille
 * 
 * @author 3407108
 *
 */
public class Emplacement {
	/**
	 * Une suite de cases qui contiennent les lettres d'un mot
	 */
	private List<Case> lettres = new ArrayList<>();

	
	/**
	 * Représentation textuelle d'un emplacement
	 *
	 * @return La chaine qui represente un emplacement
	 *
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Case c : lettres)
		{
			sb.append(c.getChar());
		}
		return sb.toString();
	}
	
	/**
	 * Retourne une suite de cases qui contiennent les lettres d'un mot
	 * 
	 * @return une suite de cases qui contiennent les lettres d'un mot
	 */
	public List<Case> getLettres(){
		return lettres;
	}
	
	/**
	 * Retourne le nombre de lettres dans un mot
	 * 
	 * @return le nombre de lettres dans un mot
	 */
	public int size() {
		return lettres.size();
	}
	
	public boolean hasCaseVide() {
		for(Case c : lettres)
		{
			if(c.isVide()) 
			{
				return true;
			}
		}
		return false;
	}
}
