package pobj.motx.tme1;

import java.util.*;

/**
 * Classe représentant tous les emplacements (des mots) d'une grille
 * 
 * @author 3407108 3872505
 *
 */
public class GrillePlaces {
	/**
	 * Liste des emplacements ordonnés de la façon suivante : mots horizontaux, mots verticaux
	 */
	private List<Emplacement> places = new ArrayList<>();
	/**
	 * Nombre d'emplacement horizontaux
	 */
	private int nbHor = 0;
	/**
	 * Nombre d'emplacement verticaux
	 */
	private int nbVert = 0;
	/**
	 * Grille staocké
	 */
	private Grille grille;

	/**
	 * Cherche tous les emplacements dans une grille donnée et les ajoute de façon ordonnée dans une liste
	 * 
	 * @param grille Grille de mots croisés
	 */
	public GrillePlaces(Grille grille) {
		this.grille = grille;
		for (int i = 0; i < grille.nbLig(); i++) {
			for (int j = 0; j < grille.nbCol(); j++) {
				if (!grille.getCase(i, j).isPleine()) {
					if (j + 1 < grille.nbCol() && !grille.getCase(i, j + 1).isPleine()) {
						Emplacement e = new Emplacement();
						while (j < grille.nbCol() && !grille.getCase(i, j).isPleine()) {
							e.getLettres().add(grille.getCase(i, j));
							j++;
						}
						places.add(e);
						nbHor++;
					}
				}
			}
		}

		for (int j = 0; j < grille.nbCol(); j++) {
			for (int i = 0; i < grille.nbLig(); i++) {
				if (!grille.getCase(i, j).isPleine()) {
					if (i + 1 < grille.nbLig() && !grille.getCase(i + 1, j).isPleine()) {
						Emplacement e = new Emplacement();
						while (i < grille.nbLig() && !grille.getCase(i, j).isPleine()) {
							e.getLettres().add(grille.getCase(i, j));
							i++;
						}
						places.add(e);
						nbVert++;
					}
				}
			}
		}
	}

	/**
	 * Retourne la liste de tous les emplacements 
	 * 
	 * @return la liste de tous les emplacements
	 */
	public List<Emplacement> getPlaces() {
		return places;
	}

	/**
	 * Retourne le nombre d'emplacement horizontaux
	 * 
	 * @return le nombre d'emplacement horizontaux
	 */
	public int getNbHorizontal() {
		return nbHor;
	}

	/**
	 * Retourne le nombre d'emplacement verticaux
	 * 
	 * @return le nombre d'emplacement verticaux
	 */
	public int getNbVertical() {
		return nbVert;
	}

	/**
	 * Représentation textuelle des emplacements d'une grille de façon ordonnée
	 * 
	 * @return la représentation textuelle des emplacements d'une grille séparés d'un saut de ligne
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int i = 1;
		for(Emplacement e : places) {
			sb.append((i++)+") "+e+"\n");
		}
		return sb.toString();
	}
	
	public GrillePlaces fixer(int m, String soluce) {
		GrillePlaces gp = new GrillePlaces(grille.copy());
		Emplacement e = gp.places.get(m);
		List<Case> cases = e.getLettres();
		for(int i =0; i<cases.size(); i++) {
			Case c = cases.get(i);
			c.setChar(soluce.charAt(i));
		}
		return gp;
	}
	
	public Grille getGrille() {
		return grille;
	}
}
