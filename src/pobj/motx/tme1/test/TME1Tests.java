package pobj.motx.tme1.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CaseTest.class, GrilleTest.class, GrillePlacesTest.class })
/**
 * Lancement de tous les tests sur les méthodes des classes
 * 
 * @author 3407108 3872505
 *
 */
public class TME1Tests {

}
