package pobj.motx.tme1;
/**
 * Classe de représentation d'une case d'une grille de mots croisés
 * 
 * @author 3407108 3872505
 *
 */
public class Case {
	/**
	 * ligne d'une case
	 */
	private int ligne;
	/**
	 * colonne d'une case
	 */
	private int colonne;
	/**
	 * contenu d'une case
	 */
	private char lettre;

	/**
	 * Construit une case avec sa position et son contenu
	 * @param ligne ligne d'une case
	 * @param colonne colonne d'une case
	 * @param lettre contenu d'une case
	 */
	public Case(int ligne, int colonne, char lettre) {
		this.ligne = ligne;
		this.colonne = colonne;
		this.lettre = lettre;
	}
	
	/**
	 * Retourne la ligne d'une case
	 * @return la ligne d'une case
	 */
	public int getLig() {
		return ligne;
	}
	
	/**
	 * Retourne la colonne d'une case
	 * @return la colonne d'une case
	 */
	public int getCol() {
		return colonne;
	}
	
	/**
	 * Retourne le contenu d'une case
	 * @return le contenu d'une case
	 */
	public char getChar() {
		return lettre;
	}
	
	/**
	 * Change le contenu d'une case
	 * @param c lettre à placer dans la case
	 */
	public void setChar(char c) {
		lettre=c;
	}
	
	/**
	 * Teste si la case n'est pas pleine et ne contient pas de lettre
	 * @return vrai si la case n'est pas pleine et ne contient pas de lettre, faux sinon
	 */
	public boolean isVide() {
		return lettre==' ';
	}
	
	/**
	 * Teste si la case est pleine
	 * @return vrai si la case est pleine, faux sinon
	 */
	public boolean isPleine() {
		return lettre=='*';
	}
}
