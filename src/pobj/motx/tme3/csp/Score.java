package pobj.motx.tme3.csp;

public class Score implements Comparable<Score> {
	private String mot;
	private int score;
	
	public Score(String mot, int score) {
		this.mot=mot;
		this.score=score;
	}
	
	public String getMot() {
		return mot;
	}
	
	public int getScore() {
		return score;
	}
	
	public int compareTo(Score sc) {
		if(score==sc.score)
			return 0;
		else
			if(score>sc.score)
				return -1;
			else 
				return 1;
	}
}
