package pobj.motx.tme3.csp;

import java.util.Collections;
import java.util.List;

public class StratAleatoire implements IChoixValeur {

	@Override
	public List<String> orderValues(ICSP problem, IVariable var) {
		List<String> mots = var.getDomain();
		if(!mots.isEmpty())	
			Collections.shuffle(mots);
		return mots;
	}

}
