package pobj.motx.tme3.csp;

import java.util.*;
import pobj.motx.tme2.*;
import pobj.motx.tme1.*;

public class MotX implements ICSP {
	private List<IVariable> vars = new ArrayList<>();
	
	public MotX(GrillePotentiel gp) {
		List<Emplacement> emplacements = gp.getGrille().getPlaces();
		int i = 0;
		
		for(Emplacement e : emplacements)
		{
			if(e.hasCaseVide())
			{
				vars.add(new DicoVariable(i, gp));
			}
			i++;
		}
	}

	@Override
	public List<IVariable> getVars() {
		return vars;
	}

	@Override
	public boolean isConsistent() {
		for(IVariable var : vars)
		{
			if(var.getDomain().isEmpty())
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public ICSP assign(IVariable vi, String val) {
		if(vi instanceof DicoVariable)
		{
			DicoVariable var = (DicoVariable) vi;
			GrillePotentiel gp = var.getGrille().fixer(var.getIndexEmplacement(),val);
			return new MotX(gp);
		}
		return null;
	}

}
