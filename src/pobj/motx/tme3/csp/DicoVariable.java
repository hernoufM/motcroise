package pobj.motx.tme3.csp;

import pobj.motx.tme2.GrillePotentiel;
import java.util.*;

public class DicoVariable implements IVariable {
	private int indEmplacement;
	private GrillePotentiel grille;

	public DicoVariable(int index, GrillePotentiel gp) {
		indEmplacement = index;
		grille = gp;
	}

	@Override
	public List<String> getDomain() {
		return grille.getMotsPot().get(indEmplacement).getMots();
	}
	
	@Override
	public String toString() {
		List<String> domaine = getDomain();
		StringBuilder sb = new StringBuilder();
		
		sb.append("Emplacement "+indEmplacement+"\n");
		sb.append("Domain: ");
		for(String s : domaine)
		{
			sb.append(s+" ");
		}
		sb.append("\n");
		return sb.toString();
	}
	
	public int getIndexEmplacement() {
		return indEmplacement;
	}
	
	public GrillePotentiel getGrille() {
		return grille;
	}
	
	public int getNombreCroisement() {
		return grille.getContrParEmpl().get(indEmplacement);
	}
}
