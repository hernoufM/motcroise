package pobj.motx.tme3.csp;

import java.util.*;

public interface IVariable {
	List<String> getDomain();
}
