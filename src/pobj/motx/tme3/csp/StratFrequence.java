package pobj.motx.tme3.csp;

import java.util.*;

public class StratFrequence implements IChoixValeur {

	private List<Map<Character, Integer>> matrice = null;
	
	@Override
	public List<String> orderValues(ICSP problem, IVariable var) {
		List<String> motsOrd = new ArrayList<>();
		if (!var.getDomain().isEmpty()) {
			matrice = new ArrayList<>(var.getDomain().get(0).length());
			for (int i = 0; i < var.getDomain().get(0).length(); i++) {
				matrice.add(new HashMap<Character, Integer>());
			}
			List<String> mots = var.getDomain();
			for (int i = 0; i < mots.size(); i++) {
				String mot = mots.get(i);
				for (int j = 0; j < mot.length(); j++) {
					if (!matrice.get(j).containsKey(mot.charAt(j))) {
						matrice.get(j).put(mot.charAt(j), 1);
					} else {
						matrice.get(j).put(mot.charAt(j), matrice.get(j).get(mot.charAt(j)) + 1);
					}
				}
			}
			List<Score> scores = new ArrayList<>(mots.size());
			for(int i =0; i<mots.size(); i++)
			{
				scores.add(new Score(mots.get(i), calculeScoreMot(mots.get(i))));
			}
			Collections.sort(scores);
			for(Score s : scores) {
				motsOrd.add(s.getMot());
			}
		}
		return motsOrd;
	}

	private int calculeScoreMot(String mot) {
		int score = 0;
		for (int i = 0; i < mot.length(); i++) {
			score += calculeScoreLettre(mot.charAt(i), i);
		}
		return score;
	}
	
	private int calculeScoreLettre(Character ch, int pos) {
		return matrice.get(pos).get(ch);
	}
}
