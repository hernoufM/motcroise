package pobj.motx.tme3.csp;

import pobj.motx.tme2.GrillePotentiel;

public class CSPSolver {
	private boolean pasAffiche=true;
	private IChoixVar stratVar=new StratFirst();
	private IChoixValeur stratVal = new StratBasic();
	
	public ICSP solve(ICSP problem) {
		//System.out.println("Solve : \n" + problem);
		// Condition terminale : succès
		if (problem.getVars().isEmpty()) {
			System.out.println("Problème résolu.");
			return problem;
		}
		// condition terminale : échec sur cette branche
		if (!problem.isConsistent()) {
			//System.out.println("Problème invalide.");
			return problem;
		} else {
			//System.out.println("Problème valide.");
		}
		// On choisit une variable arbitraire, ici la première
		// On est garantis que ! getVars().isEmpty(), testé au dessus
		IVariable vi = stratVar.chooseVar(problem);

		ICSP next = null;
		// On est garantis que toute variable a un domaine non nul
		for (String val : stratVal.orderValues(problem, vi)) {
			//System.out.println("Fixe var : Emplacement" + ((DicoVariable) vi).getIndexEmplacement() + " à " + val);
			next = problem.assign(vi, val);
			next = solve(next);
			if (next.isConsistent()) {
				/*affichage d'une soltion comme une grille une fois*/
				if (pasAffiche) {
					if (vi instanceof DicoVariable) {
						DicoVariable dv = (DicoVariable) vi;
						GrillePotentiel gp = dv.getGrille().fixer(dv.getIndexEmplacement(), val);
						System.out.println("\n\n"+gp.getGrille().getGrille()+"\n");
						pasAffiche=false;
					}
				}
				return next;
			} else {
				//System.out.println("Essai valeur suivante.");
			}
		}
		//System.out.println("Backtrack sur variable " + vi);
		return next;
	}
	
	public void setChoixVarStrat(IChoixVar strat) {
		stratVar=strat;
	}
	
	public void setChoixValStrat(IChoixValeur strat) {
		stratVal=strat;
	}
}
