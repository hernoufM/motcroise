package pobj.motx.tme3.csp;

import java.util.*;

public interface IChoixValeur {
	List<String> orderValues(ICSP problem, IVariable var);
}
