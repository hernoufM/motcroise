package pobj.motx.tme3.csp;

public class StratFirst implements IChoixVar {

	@Override
	public IVariable chooseVar(ICSP problem) {
		if(problem!=null && !problem.getVars().isEmpty())
		{
			return problem.getVars().get(0);
		}
		return null;
	}

}
