package pobj.motx.tme3.csp;

public class StratMaxContraintes implements IChoixVar {

	@Override
	public IVariable chooseVar(ICSP problem) {
		if (problem != null && !problem.getVars().isEmpty()) {
			DicoVariable var = (DicoVariable) problem.getVars().get(0);
			for(int i=0; i<problem.getVars().size(); i++)
			{
				DicoVariable v = (DicoVariable) problem.getVars().get(i);
				if(var.getNombreCroisement()<v.getNombreCroisement())
					var=v;
			}
			return var;
		}
		return null;
	}

}
