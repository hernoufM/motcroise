package pobj.motx.tme3.csp;

public class StratMin implements IChoixVar {

	@Override
	public IVariable chooseVar(ICSP problem) {
		if(problem!=null && !problem.getVars().isEmpty())
		{
			IVariable var = problem.getVars().get(0);
			for(IVariable v : problem.getVars()) {
				if(v.getDomain().size()<var.getDomain().size())
					var=v;
			}
			return var;
		}
		return null;
	}

}
