package pobj.motx.tme3.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class) 
@SuiteClasses({ GrilleSolverTest.class, GrilleSolverTest1.class, GrilleSolverTest2.class, GrilleSolverTest3.class, GrilleSolverTest4.class,
 GrilleSolverTest6.class, GrilleSolverTest7.class})
public class TME3Tests {

}
