package pobj.motx.tme3.test;

import org.junit.Test;

import pobj.motx.tme1.Grille;
import pobj.motx.tme1.GrilleLoader;
import pobj.motx.tme1.GrillePlaces;
import pobj.motx.tme2.Dictionnaire;
import pobj.motx.tme2.GrillePotentiel;
import pobj.motx.tme3.csp.MotX;
import pobj.motx.tme3.csp.StratAleatoire;
import pobj.motx.tme3.csp.StratBasic;
import pobj.motx.tme3.csp.StratFrequence;
import pobj.motx.tme3.csp.StratMaxContraintes;
import pobj.motx.tme3.csp.StratMin;
import pobj.motx.tme3.csp.CSPSolver;
import pobj.motx.tme3.csp.ICSP;

public class GrilleSolverTest2 {

	@Test
	public void testEnonceDefault() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");
		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);
		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceFirstAleatoire() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixValStrat(new StratAleatoire());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceFirstFrequence() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixValStrat(new StratFrequence());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceMinBasic() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixVarStrat(new StratMin());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceMinAleatoire() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixVarStrat(new StratMin());
		solver.setChoixValStrat(new StratAleatoire());
		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceMinFrequence() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixVarStrat(new StratMin());
		solver.setChoixValStrat(new StratFrequence());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceMaxContraintesBasic() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixVarStrat(new StratMaxContraintes());
		solver.setChoixValStrat(new StratBasic());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceMaxContraintesAleatoire() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixVarStrat(new StratMaxContraintes());
		solver.setChoixValStrat(new StratAleatoire());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

	@Test
	public void testEnonceMaxContraintesFrequence() {
		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/enonce.grl");

		System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();
		solver.setChoixVarStrat(new StratMaxContraintes());
		solver.setChoixValStrat(new StratFrequence());

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());

		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(
				"Solution \n" + solution + " \nCalculée en " + (System.currentTimeMillis() - timestamp) + " ms ");
	}

}
