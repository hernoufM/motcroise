package pobj.motx.tme2;

import java.util.*;

public class CroixContrainte implements IContrainte {
	
	private int m1;
	private int c1;
	private int m2;
	private int c2;
	
	public CroixContrainte(int m1, int c1, int m2, int c2) {
		this.m1=m1;
		this.m2=m2;
		this.c1=c1;
		this.c2=c2;
	}
	
	@Override
	public int reduce(GrillePotentiel gp) {
		List<Dictionnaire> motsPot = gp.getMotsPot();
		Dictionnaire d1 = motsPot.get(m1);
		Dictionnaire d2 = motsPot.get(m2);
		
		EnsembleLettre ens1 = d1.calculEnsembleLettre(c1);
		EnsembleLettre ens2 = d2.calculEnsembleLettre(c2);
		EnsembleLettre inter = ens1.intersection(ens2);
		
		int nb = 0;
		if(ens1.size()>inter.size())
			nb = d1.filtreParEnsLettres(inter, c1);
		if(ens2.size()>inter.size())
			nb += d2.filtreParEnsLettres(inter, c2);
		return nb;
	}
	
	@Override 
	public boolean equals(Object o) {
		if(! (o instanceof CroixContrainte))
			return false;
		CroixContrainte c = (CroixContrainte) o;
		return (m1==c.m1 && c1==c.c1 && m2==c.m2 && c2==c.c2);
	}
}
