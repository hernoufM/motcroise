package pobj.motx.tme2;
import java.util.*;

public class EnsembleLettre {
	private List<Character> lettres = new ArrayList<>();
	
	public boolean add(char c) {
		if(lettres.contains(c))
			return false;
		return lettres.add(c);
	}
	
	public int size() {
		return lettres.size();
	}
	
	public boolean contains(char c) {
		return lettres.contains(c);
	}
	
	public EnsembleLettre intersection(EnsembleLettre ens) {
		EnsembleLettre inter = new EnsembleLettre();
		for(Character c : lettres) {
			if(ens.contains(c)) {
				inter.add(c);
			}
		}
		return inter;
	}
	
	public List<Character> getLettres(){
		return lettres;
	}
}
