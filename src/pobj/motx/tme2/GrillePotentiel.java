package pobj.motx.tme2;

import pobj.motx.tme1.*;
import java.util.*;

public class GrillePotentiel {
	private GrillePlaces grille;
	private Dictionnaire dictionnaire;
	private List<Dictionnaire> motsPot = new ArrayList<>();
	private List<IContrainte> contraintes = new ArrayList<>();
	private List<Integer> contraintesParEmplacement;

	public GrillePotentiel(GrillePlaces grille, Dictionnaire dicoComplet) {
		this.grille = grille;
		dictionnaire = dicoComplet;
		contraintesParEmplacement = new ArrayList<>();
		for (int i = 0; i < grille.getNbHorizontal() + grille.getNbVertical(); i++) {
			contraintesParEmplacement.add(i, 0);
		}

		List<Emplacement> emplacements = grille.getPlaces();
		for (Emplacement e : emplacements) {
			Dictionnaire dic = dictionnaire.copy();
			dic.filtreLongueur(e.size());
			List<Case> cases = e.getLettres();
			for (Case c : cases) {
				if (c.getChar() != ' ') {
					dic.filtreParLettre(c.getChar(), cases.indexOf(c));
				}
			}
			//Suprimé car diminué vitesse de calcul
			/*if (dic.getMots().size() == 1)
				contraintes.add(new MotUnique(this.grille.getPlaces().indexOf(e),dic.get(0)));*/
			motsPot.add(dic);
		}

		int nbH = grille.getNbHorizontal();
		int nbV = grille.getNbVertical();
		List<Emplacement> empH = emplacements.subList(0, nbH);
		List<Emplacement> empV = emplacements.subList(nbH, nbH + nbV);
		for (Emplacement eh : empH) {
			for (Case c : eh.getLettres()) {
				for (Emplacement ev : empV) {
					if (ev.getLettres().contains(c) && c.isVide()) {
						int i = emplacements.indexOf(eh), j = emplacements.indexOf(ev);
						contraintes
								.add(new CroixContrainte(i, eh.getLettres().indexOf(c), j, ev.getLettres().indexOf(c)));
						contraintesParEmplacement.set(i, contraintesParEmplacement.get(i) + 1);
						contraintesParEmplacement.set(j, contraintesParEmplacement.get(j) + 1);
					}
				}
			}
		}
		propage();
	}

	public GrillePotentiel(GrillePlaces grille, Dictionnaire dicoComplet, List<Dictionnaire> motsPot) {
		this.grille = grille;
		dictionnaire = dicoComplet;
		contraintesParEmplacement = new ArrayList<>();
		for (int i = 0; i < grille.getNbHorizontal() + grille.getNbVertical(); i++) {
			contraintesParEmplacement.add(i, 0);
		}

		List<Emplacement> emplacements = grille.getPlaces();
		int i = 0;
		for (Emplacement e : emplacements) {
			Dictionnaire dic = motsPot.get(i).copy();
			List<Case> cases = e.getLettres();
			for (Case c : cases) {
				if (c.getChar() != ' ') {
					dic.filtreParLettre(c.getChar(), cases.indexOf(c));
				}
			}
			//Suprimé car diminiu vitesse de calcul
			/*if (dic.getMots().size() == 1)
				contraintes.add(new MotUnique(i,dic.get(0)));*/
			this.motsPot.add(dic);
			i++;
		}

		int nbH = grille.getNbHorizontal();
		int nbV = grille.getNbVertical();
		List<Emplacement> empH = emplacements.subList(0, nbH);
		List<Emplacement> empV = emplacements.subList(nbH, nbH + nbV);
		for (Emplacement eh : empH) {
			for (Case c : eh.getLettres()) {
				for (Emplacement ev : empV) {
					if (ev.getLettres().contains(c) && c.isVide()) {
						i = emplacements.indexOf(eh);
						int j = emplacements.indexOf(ev);
						contraintes.add(new CroixContrainte(emplacements.indexOf(eh), eh.getLettres().indexOf(c),
								emplacements.indexOf(ev), ev.getLettres().indexOf(c)));
						contraintesParEmplacement.set(i, contraintesParEmplacement.get(i) + 1);
						contraintesParEmplacement.set(j, contraintesParEmplacement.get(j) + 1);
					}
				}
			}
		}
		propage();
	}

	public boolean isDead() {
		for (Dictionnaire dic : motsPot) {
			if (dic.size() == 0)
				return true;
		}
		return false;
	}

	public List<Dictionnaire> getMotsPot() {
		return motsPot;
	}

	public GrillePlaces getGrille() {
		return grille;
	}

	public GrillePotentiel fixer(int m, String soluce) {
		GrillePotentiel gp = new GrillePotentiel(grille.fixer(m, soluce), dictionnaire, motsPot);
		return gp;
	}

	public List<IContrainte> getContraintes() {
		return contraintes;
	}

	public List<Integer> getContrParEmpl() {
		return contraintesParEmplacement;
	}

	private boolean propage() {
		int cpt = 0;
		while (true) {
			for (IContrainte c : contraintes) {
				cpt += c.reduce(this);
			}
			if (isDead()) {
				return false;
			}
			if (cpt == 0) {
				return true;
			}
			cpt = 0;
		}
	}
}
