package pobj.motx.tme2;

import java.util.List;

public class MotUnique implements IContrainte {
	private int indexEmplacement; 
	private String mot;
	
	public MotUnique(int index, String s) {
		indexEmplacement =index;
		mot=s;
	}
	
	@Override
	public int reduce(GrillePotentiel gp) {
		List<Dictionnaire> motPots = gp.getMotsPot();
		int nb = 0;
		for(int i=0; i<motPots.size(); i++)
		{
			if(i!=indexEmplacement)
				if(motPots.get(i).getMots().remove(mot))
				{
					/*motPots.get(i).invaliderCache();*/
					nb++;
				}
		}
		return nb;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof MotUnique))
			return false;
		return mot.equals(((MotUnique) o).mot) && indexEmplacement==((MotUnique) o).indexEmplacement;
	}
}
