package pobj.motx.tme2;

import java.util.ArrayList;
import java.util.List;
import java.io.*;

/**
 * Un ensemble de mots.
 *
 */
public class Dictionnaire {

	// stockage des mots
	private List<String> mots = new ArrayList<>();

	private EnsembleLettre[] cache = null;

	/**
	 * Ajoute un mot au Dictionnaire, en dernière position.
	 * 
	 * @param mot
	 *            à ajouter, il sera stocké en minuscules (lowerCase)
	 */
	public void add(String mot) {
		mots.add(mot.toLowerCase());
	}

	/**
	 * Taille du dictionnaire, c'est à dire nombre de mots qu'il contient.
	 * 
	 * @return la taille
	 */
	public int size() {
		return mots.size();
	}

	/**
	 * Accès au i-eme mot du dictionnaire.
	 * 
	 * @param i
	 *            l'index du mot recherché, compris entre 0 et size-1.
	 * @return le mot à cet index
	 */
	public String get(int i) {
		return mots.get(i);
	}

	/**
	 * Rend une copie de ce Dictionnaire.
	 * 
	 * @return une copie identique de ce Dictionnaire
	 */
	public Dictionnaire copy() {
		Dictionnaire copy = new Dictionnaire();
		copy.mots.addAll(mots);
		if (cache == null)
			copy.cache = null;
		else {
			copy.cache = new EnsembleLettre[cache.length];
			for (int i = 0; i < cache.length; i++) {
				if (cache[i] == null)
					copy.cache[i] = null;
				else {
					EnsembleLettre ens = new EnsembleLettre();
					for (int j = 0; j < cache[i].getLettres().size(); j++) {
						ens.add(cache[i].getLettres().get(j));
					}
					copy.cache[i]=ens;
				}
			}
		}
		return copy;
	}

	/**
	 * Retire les mots qui ne font pas exactement "len" caractères de long.
	 * Attention cette opération modifie le Dictionnaire, utiliser copy() avant de
	 * filtrer pour ne pas perdre d'information.
	 * 
	 * @param len
	 *            la longueur voulue
	 * @return le nombre de mots supprimés
	 */
	public int filtreLongueur(int len) {
		List<String> cible = new ArrayList<>();
		int cpt = 0;
		for (String mot : mots) {
			if (mot.length() == len)
				cible.add(mot);
			else
				cpt++;
		}
		mots = cible;
		return cpt;
	}

	@Override
	public String toString() {
		if (size() == 1) {
			return mots.get(0);
		} else {
			return "Dico size =" + size();
		}
	}

	public static Dictionnaire loadDictionnaire(String path) {
		Dictionnaire dic = new Dictionnaire();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				dic.add(line);
				;
			}
		} catch (IOException e) {
			// Problème d’accès au fichier.
			e.printStackTrace();
		}

		return dic;
	}

	public int filtreParLettre(char c, int i) {
		List<String> mots2 = new ArrayList<>();
		int nb = 0;
		for (String s : mots) {
			if (s.charAt(i) == c)
				mots2.add(s);
			else
				nb++;
		}
		mots = mots2;
		if (nb > 0)
			cache = null;
		return nb;
	}

	public EnsembleLettre calculEnsembleLettre(int pos) {
		EnsembleLettre ens = new EnsembleLettre();
		if (mots.isEmpty())
			return ens;
		if (cache == null)
			cache = new EnsembleLettre[mots.get(0).length()];
		if (cache[pos] == null) {
			for (String s : mots) {
				ens.add(s.charAt(pos));
			}
			cache[pos] = ens;
			return ens;
		} else
			return cache[pos];
	}

	public int filtreParEnsLettres(EnsembleLettre ens, int pos) {
		List<String> mots2 = new ArrayList<>();
		int nb = 0;
		for (String s : mots) {
			if (ens.contains(s.charAt(pos)))
				mots2.add(s);
			else
				nb++;
		}
		mots = mots2;
		if (nb > 0)
			cache = null;
		return nb;
	}

	public List<String> getMots() {
		return mots;
	}
	
	public void invaliderCache() {
		cache =null;
	}
}
